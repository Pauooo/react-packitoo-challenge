import axios from 'axios';

// types
export const PRODUCTS_FETCHED = 'PRODUCTS_FETCHED';

// Actions
export const productsFetched = (products) => dispatch => {
  dispatch({
    type:  PRODUCTS_FETCHED,
    products
  });
};


export function fetchProducts(url){
  return(dispatch) => {
    return axios
      .get(url)
      .then(response => {
        dispatch(productsFetched(response.data));
      });
  };
}
