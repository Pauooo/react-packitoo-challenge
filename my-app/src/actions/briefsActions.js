import axios from 'axios';

// Types
export const SAVED_BRIEF ='SAVE_BRIEF';
export const BRIEFS_FETCHED = 'BRIEFS_FETCHED';
export const LOAD_BRIEFS = 'LOAD_BRIEFS';

// Actions
export const savedBrief = (brief) => dispatch => {
  dispatch({
    type: SAVED_BRIEF,
    brief
  });
};


export const briefsFetched = (briefs) => dispatch => {
  dispatch({
    type:  BRIEFS_FETCHED,
    briefs,
    loaded: true
  });
};

export const loadBriefs = () => dispatch => {
  dispatch({
    type:  LOAD_BRIEFS,
    loaded: false
  });
};

export function saveBrief(url, values){
  return(dispatch) => {
    return axios
      .post(url,
        {
          title: values.title,
          comment: values.comment,
          productId: Number(values.productId)
        }
      )
      .then(response => {
        dispatch(savedBrief(response.data));
        dispatch(fetchBriefs('http://localhost:3000/briefs'));
      });
  };
}

export function fetchBriefs(url){
  return(dispatch) => {
    dispatch(loadBriefs());
    return axios
      .get(url)
      .then(response => {
        dispatch(briefsFetched(response.data));
      });
  };
}
