import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import { brief, briefs, loaded }   from './briefsReducer';
import productsReducer from './productsReducer';

export default combineReducers({
  briefs,
  brief,
  loaded,
  products: productsReducer,
  form: formReducer,
});
