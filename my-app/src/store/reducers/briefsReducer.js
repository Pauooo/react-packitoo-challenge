import { SAVED_BRIEF, BRIEFS_FETCHED, LOAD_BRIEFS } from '../../actions/briefsActions';


export const brief = (state = {}, action) => {
  switch (action.type) {
    case SAVED_BRIEF:
      return action.brief;
    default:
      return state;
  }
};


export const briefs = (state = [], action) => {
  switch (action.type) {
    case BRIEFS_FETCHED:
      return action.briefs;

    default:
      return state;
  }
};

export const loaded = (state = false, action) => {
  switch (action.type) {
    case LOAD_BRIEFS:
      return  action.loaded;

    case BRIEFS_FETCHED:
      return  action.loaded;
    default:
      return state;
  }
};
