import { PRODUCTS_FETCHED } from '../../actions/productsActions';

export default (state = [], action) => {
  switch (action.type) {
    case PRODUCTS_FETCHED:
      return action.products;
    default:
      return state;
  }
};
