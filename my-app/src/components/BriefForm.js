import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import { Paper, Typography } from '@material-ui/core';

import { fetchProducts } from '../actions/productsActions';
import { saveBrief } from '../actions/briefsActions';
import Form from './Form';

const styles = theme => ({
  formLayout: {
    marginTop: theme.spacing.unit * 3,
    padding: theme.spacing.unit * 2,
    [theme.breakpoints.up(600 + theme.spacing.unit * 3 * 2)]: {
      width: 600,
      marginLeft: 'auto',
      marginRight: 'auto',
      marginTop: theme.spacing.unit * 6,
      marginBottom: theme.spacing.unit * 6,
      padding: theme.spacing.unit * 3
    }
  }
});

class StyledBriefForm extends React.Component {
  // Lifecycles

  componentDidMount() {
    this.props.fetchProducts('http://localhost:3000/products');
  }

  // Handlers

  handleSubmit = values => {
    this.props.saveBrief('http://localhost:3000/briefs', values);
  };

  // Form

  render() {
    const { products, classes } = this.props;
    return (
      <Paper elevation={2} className={classes.formLayout}>
        <Typography component="h2" variant="h4" align="center">
          Add a new brief
        </Typography>
        <Form products={products} onSubmit={this.handleSubmit} />
      </Paper>
    );
  }
}

StyledBriefForm.propTypes = {
  fetchProducts: PropTypes.func.isRequired,
  saveBrief: PropTypes.func.isRequired,
  products: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
  products: state.products
});

const mapDispatchToProps = dispatch => ({
  fetchProducts: url => dispatch(fetchProducts(url)),
  saveBrief: (url, values) => dispatch(saveBrief(url, values))
});

const BriefForm = withStyles(styles)(StyledBriefForm);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BriefForm);
