import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Card, CardContent, Chip, Grid, Typography } from '@material-ui/core';

const styles = theme => ({
  chip: {
    marginTop: theme.spacing.unit * 2
  }
});

const BriefCard = props => (
  <Grid item sm={6} md={4} lg={3}>
    <Card>
      <CardContent>
        <Typography variant="h5" color="primary">
          {props.title}
        </Typography>
        <Typography variant="body1">{props.comment}</Typography>
        <Chip className={props.classes.chip} label={props.productName} />
      </CardContent>
    </Card>
  </Grid>
);

BriefCard.propTypes = {
  title: PropTypes.string.isRequired,
  comment: PropTypes.string.isRequired,
  productName: PropTypes.string.isRequired
};

export default withStyles(styles)(BriefCard);
