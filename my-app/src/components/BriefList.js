import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { Grid, Typography } from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';

import BriefCard from './BriefCard';
import FilterSelect from './FilterSelect';
import { fetchBriefs } from '../actions/briefsActions';
import { fetchProducts } from '../actions/productsActions';

const styles = theme => ({
  progress: {
    margin: theme.spacing.unit * 2
  },
  layout: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
      width: 1100,
      marginLeft: 'auto',
      marginRight: 'auto'
    }
  },
  cardGrid: {
    padding: `${theme.spacing.unit * 8}px 0`
  },
  title: {
    marginBottom: theme.spacing.unit * 3
  }
});

class StyledBriefList extends React.Component {
  state = {
    product: {
      productId: 'all'
    }
  };

  // Lifecycles
  componentDidMount() {
    this.props.fetchBriefs('http://localhost:3000/briefs');
    this.props.fetchProducts('http://localhost:3000/products');
  }

  findProductName = productId => {
    const { products } = this.props;
    let productFinded = products.find(product => product.id === productId);
    return productFinded.name;
  };

  handleFilter = product => {
    this.setState({ product });
  };

  // List
  render() {
    const { loaded, briefs, products, classes } = this.props;
    const { product } = this.state;
    const filteredBriefs =
      product.productId === 'all'
        ? briefs
        : briefs.filter(brief => brief.productId === product.productId);
    return (
      <div className={classNames(classes.layout, classes.cardGrid)}>
        <Typography
          component="h2"
          variant="h4"
          align="left"
          className={classes.title}
        >
          Briefs List
        </Typography>
        {!loaded && <CircularProgress className={classes.progress} />}
        {loaded && (
          <Fragment>
            <FilterSelect products={products} onSubmit={this.handleFilter} />
            <Grid container spacing={24}>
              {filteredBriefs.map(brief => (
                <BriefCard
                  key={brief.id}
                  title={brief.title}
                  comment={brief.comment}
                  productName={this.findProductName(brief.productId)}
                />
              ))}
            </Grid>
          </Fragment>
        )}
      </div>
    );
  }
}

StyledBriefList.propTypes = {
  products: PropTypes.array.isRequired,
  fetchBriefs: PropTypes.func.isRequired,
  loaded: PropTypes.bool,
  briefs: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
      comment: PropTypes.string.isRequired,
      productId: PropTypes.number.isRequired
    }).isRequired
  ).isRequired
};

const mapStateToProps = state => ({
  products: state.products,
  briefs: state.briefs,
  loaded: state.loaded
});

const mapDispatchToProps = dispatch => ({
  fetchBriefs: url => dispatch(fetchBriefs(url)),
  fetchProducts: url => dispatch(fetchProducts(url))
});

const BriefList = withStyles(styles)(StyledBriefList);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BriefList);
