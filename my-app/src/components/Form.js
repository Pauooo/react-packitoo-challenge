import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field, reset } from 'redux-form';
import {
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  TextField,
  Select
} from '@material-ui/core';
import withStyles from '@material-ui/core/styles/withStyles';

const styles = theme => ({
  form: {
    margin: theme.spacing.unit,
    minWidth: 120,
    display: 'flex',
    flexDirection: 'column'
  },
  button: {
    marginTop: theme.spacing.unit * 3,
    marginLeft: theme.spacing.unit
  }
});

// Making sure that all the fields has a value
const validate = values => {
  const errors = {};
  if (!values.title) {
    errors.title = 'Required';
  }
  if (!values.comment) {
    errors.comment = 'Required';
  }
  if (!values.productId) {
    errors.productId = 'Required';
  }
  return errors;
};

//  Function to render the fields
const createRenderer = render => ({
  input,
  meta,
  label,
  products,
  classes,
  ...rest
}) => (
  <FormControl className={classes.form}>
    {render(input, label, meta, rest, products)}
  </FormControl>
);

createRenderer.propTypes = {
  input: PropTypes.string,
  meta: PropTypes.object,
  label: PropTypes.string
};

//  Rendering the input fields
const RenderInput = createRenderer((input, label, meta) => (
  <TextField
    label={label}
    fullWidth
    required
    error={meta.error && meta.touched}
    {...input}
  />
));

//  Rendering the select field
const RenderSelect = createRenderer(
  (input, label, meta, { children }, products) => (
    <Fragment>
      <InputLabel>{label}</InputLabel>
      <Select fullWidth required error={meta.error && meta.touched} {...input}>
        {products.map(product => (
          <MenuItem key={product.id} value={product.id}>
            {product.name}
          </MenuItem>
        ))}
      </Select>
    </Fragment>
  )
);

//  Form
let StyledForm = props => {
  const { handleSubmit, submitting, products, classes } = props;
  return (
    <form onSubmit={handleSubmit}>
      <Field
        name="title"
        label="Title"
        classes={classes}
        component={RenderInput}
      />
      <Field
        name="comment"
        label="Comment"
        classes={classes}
        component={RenderInput}
      />
      <Field
        name="productId"
        label="Product"
        products={products}
        classes={classes}
        component={RenderSelect}
      />
      <Button
        className={classes.button}
        variant="contained"
        color="primary"
        type="submit"
        disabled={submitting}
      >
        Submit
      </Button>
    </form>
  );
};

const clearForm = (result, dispatch) => dispatch(reset('briefForm'));

StyledForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
  products: PropTypes.array.isRequired
};

let Form = withStyles(styles)(StyledForm);

Form = reduxForm({
  form: 'briefForm',
  validate,
  onSubmitSuccess: clearForm
})(Form);

export default Form;
