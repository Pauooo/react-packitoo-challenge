import React, { Fragment } from 'react';
import { AppBar, Typography, Toolbar } from '@material-ui/core';

const Header = () => (
  <Fragment>
    <AppBar position="static" color="primary">
      <Toolbar>
        <Typography component="h1" variant="h4" color="inherit">
          Briefs
        </Typography>
      </Toolbar>
    </AppBar>
  </Fragment>
);

export default Header;
