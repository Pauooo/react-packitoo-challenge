import React, { Fragment } from 'react';

import Header from './Header';
import BriefForm from './BriefForm';
import BriefList from './BriefList';

const App = () => (
  <Fragment>
    <Header />
    <BriefForm />
    <BriefList />
  </Fragment>
);

export default App;
