import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field, reset } from 'redux-form';
import {
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Typography,
  Select
} from '@material-ui/core';
import withStyles from '@material-ui/core/styles/withStyles';

const styles = theme => ({
  filterForm: {
    backgroundColor: theme.palette.grey[200],
    padding: theme.spacing.unit * 2,
    margin: theme.spacing.unit * 2
  },
  filterTitle: {
    marginLeft: theme.spacing.unit
  },
  form: {
    marginBottom: theme.spacing.unit,
    minWidth: 120,
    display: 'flex',
    flexDirection: 'column'
  }
});

const createRenderer = render => ({
  input,
  meta,
  label,
  products,
  classes
}) => (
  <FormControl className={classes.form}>
    {render(input, label, meta, products)}
  </FormControl>
);

//  Rendering the select field
const RenderSelect = createRenderer((input, label, meta, products) => (
  <Fragment>
    <InputLabel>{label}</InputLabel>
    <Select required error={meta.error && meta.touched} {...input}>
      <MenuItem value="all">All</MenuItem>
      {products.map(product => (
        <MenuItem key={product.id} value={product.id}>
          {product.name}
        </MenuItem>
      ))}
    </Select>
  </Fragment>
));

//  Form
let BriefsFilterSelect = props => {
  const { handleSubmit, submitting, products, classes } = props;
  return (
    <form onSubmit={handleSubmit} className={classes.filterForm}>
      <Typography className={classes.filterTitle} variant="h6">
        Filter by product
      </Typography>
      <Field
        name="productId"
        label="Product"
        products={products}
        classes={classes}
        component={RenderSelect}
      />
      <Button
        variant="contained"
        color="primary"
        type="submit"
        disabled={submitting}
      >
        Filter
      </Button>
    </form>
  );
};

const clearForm = (result, dispatch) => dispatch(reset('briefsFilter'));

BriefsFilterSelect.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
  products: PropTypes.array.isRequired
};

let FilterSelect = withStyles(styles)(BriefsFilterSelect);

FilterSelect = reduxForm({
  form: 'briefsFilter',
  onSubmitSuccess: clearForm
})(FilterSelect);

export default FilterSelect;
